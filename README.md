# FAAO


##Table des matières
- Todo List
- Rapport de tests


---------------------------------------------

##Todo List

###Version dépôt de mémoire de maîtrise:
- **Récupérer données des dropdown à partir de javascript** (Seb)
- **Compatibilité avec Firefox, IE, Mac, Safari, iPad.** (Seb)

###Version ultérieure court terme: 
- Réintégation du synth de guitare (Seb)
- MAJ du moteur pour la gestion sans l'index des tableaux de formules, de gammes et de mode.
- MAJ de abc2degre pour la gestion des altérations des degrés.
- Gestion des bécarres dans la formule 6717-6-5 en Mi, La, Si, Fa# et Do# mineur.
- Intégration de l'armature.
- Intégration de textes: titre, tempo, barre finale, copyright.
- Intégration d'un régulateur de tempo.
- Intégration d'un quatrième octave abc pour la gestion des formules mineures.

###Version ultérieure long terme: 
- Séries d'exercices de lecture de notes.
- Gestion de la conduite de voix de l'accompagnement.
- Affichage de la partition de l'accompagnement.
- Module permettant de devenir millionnaires en 15 minutes en appuyant sur un bouton. (lol)

###Tâches complétées:
- Création d'un dropdown button. (Seb) (2016-12-06)
- Insertion d'un bootstrap. (Seb) (2016-12-06)
- Affichage des altérations avec VexFlow. (Seb) (2016-12-06)
- Création d'un package. (seb) (2016-12-07)
- Installation des tonalité mineures. (2016-12-07?)
- Démarrage de l'exercice 3. (2016-12-07)
- Installation de la tonalité. (2016-12-13)
- Finalisation de la partie interface de l'exercice 1. (2016-12-13)
- Accompagnement audio de l'exercice 1. (2016-12-13)
- Intégration du midi player. (2016-12-14)
- Régler le bugs de tonalités inexistantes. (2016-12-14)
- Création d'un check box. (Seb) (2016-12-15)
- Option MIDI pour le convertisseurAbc. (2016-12-15)
- Intégrer l'audio de l'exercice 2. (2016-12-15)
- Conversion du player bloquant à non-bloquant. (2016-12-18)
- Interruption du player à mi-parcours. (2016-12-18)
- Correction partielle de bugs liés aux boutons. (2016-12-18)
- Intégration du checkbox auto de l'affichage. (2016-12-18)
- Intégration du checkbox auto de l'installation de la tonalité. (2016-12-18)
- Correction partielle de bugs liés aux boutons. (2016-12-19)
- MAJ de la gestion des boutons exercice 1. (2016-12-19)
- MAJ de la gestion des boutons exercice 2. (2016-12-19)
- Exercice 1 terminé. (2016-12-19)
- Exercice 2 terminé. (2016-12-19)
- Intégration du degré sur partition de l'exercice 3. (2016-12-19)
- Correction bugs dans "jouer la réponse". (2016-12-19)
- Exercice 3 terminé. (2016-12-19)
- MAJ des commentaires dans global.js et ex1.js. (2016-12-19)
- Installation d'un serveur pour des tests à distance. 
- Début des tests avec un débutant. 

---------------------------------------------
##Rapport de tests

###Test #1: 15 décembre 2016 
- Système: IPad + chrome (version récente mais inconnue).
- Ce qui marche: les boutons, les dropdown, les partitions, les bonnes notes, le bootstrap.
- Ce qui ne marche pas: le son.

###Test #2: 15 décembre 2016
- Système: PC + Firefox (v. 48.0).
- Ce qui marche: les boutons, les dropdown, les partitions, les bonnes notes, le bootstrap.
- Ce qui ne marche pas: le son. Comportement étrange, "pop" de notes jouées rarement et simultanément avec une très courte durée.