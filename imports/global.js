/**
 * FAAO 0.1 built on 2016-12-08.
 * Copyright (c) 2016 Simon Desautels <simon.desautels.music@gmail.com>
 *
 */
// global.js
//
// ## Description
//
// Outils de gestion des formules et des tonalités.
//
MIDI_BASE = 60;	// Do central à 60. Constante de tranposition.
DELAI_DEPART = 200; // Délai avant que les notes sont jouées. 
SEUIL_OCTAVE = 0; // Seuil qui détermine l'ajustement des gammes trop aïgus. 1 à 7. Défaut: 0.
SAUT_OCTAVE = 7; // Saut d'un octave pour la transposition. 1 octave = 7 degrés. Défauf: 7.
REGISTRE = 4; // Hauteur de note pour le VexFlow. Défaut: 4.
ABC_ALT = "_=^"; // Les caractères possibles utilisés comme altérations du langage ABC.
ABC_ALT_TAB = ["__", "_", "=", "^", "^^"]; // Tableau des altérations du langage ABC.
VEX_ALT = "bn#"; // Les caractères possibles utilisés comme altérations du langage Vex.
ABC2VEX_ALT = {"_":"b", "=":"n", "^":"#"}; // Correspondance des altérations entre ABC et VEX.
ABC2LAT_NOTES = {"C":"Do", "D":"Ré", "E":"Mi", "F":"Fa", "G":"Sol", "A":"La", "B":"Si"}; // Correspondance notes latines et Vex.
CVT_LATIN = 0; // Option du convertisseur en notes latines.
CVT_VEX = 1; // Option du convertisseur en notes Vex.
CVT_MIDI = 2; // Option du convertisseur en notes MIDI.
VEX_SPACE_BETWEEN_STAVES = 110; // Espace en pixel entre les portées. Défaut: 110.
VEX_STAVES_WIDTH = 200; // Largeur de portée en pixel. Défaut: 200.
VEX_LARGE_STAVES_WIDTH_RATIO = 1.5; // Ratio d'ajustement de portée. Défaut: 1.5.
VEX_NOTE_DURATION = "q"; // Note par défaut de l'affichage des notes. Défaut: "q".
DROPDOWN_TONALITE = ["Do","Ré","Mi","Fa","Sol","La","Si","Dob","Réb","Mib","Solb","Lab","Sib","Do#","Ré#","Fa#","Sol#","La#"];
DROPDOWN_MODE = ["Majeur", "Mineur"];
DROPDOWN_FORMULES_MAJ = ["21", "321", "5151", "4321", "5671", "671", "61", "71"];
DROPDOWN_FORMULES_MIN = ["21", "3-21", "5151", "43-21", "6-51a", "6-51b", "6-1", "61", "7-1", "71", "6-7-1", "6-71", "6717-6-5"];
formules = [ // Formules de stimulation du sens tonal en Do majeur et mimneur.
	["dc", "edc", "gcGc", "fedc", "GABc", "ABc", "Ac", "Bc"],
	["dc", "_edc", "gcGc", "f_edc", "_agc", "_AGc", "_Ac", "=Ac", "_Bc", "=Bc", "_A_Bc", "_A=Bc", "=A=Bc_B_AG"]
];
MIDI_DOMAJ = [0, 2, 4, 5, 7, 9, 11]; // Gamme majeure relative en MIDI.
MIDI_NOTES = ["C", "D", "E", "F", "G", "A", "B"]; // Gamme majeure en notes pour le convertisseur.
SUITE_NOTES = ["C,", "D,", "E,", "F,", "G,", "A,", "B,", // Suite des notes ABC.
	"C", "D", "E", "F", "G", "A", "B", "c", "d", "e", "f", "g", "a", "b"];
SUITE_ALT_MAJ = [7, 3, 6, 2, 5, 1, 4]; // Suite d'altérations en majeur. Défaut: [7, 3, 6, 2, 5, 1, 4].
SUITE_ALT_MIN = [2, 5, 1, 4, 7, 3, 6]; // Suite d'altérations en mineur. Défaut: [2, 5, 1, 4, 7, 3, 6].

choixTonalite = -1; // Variable globale qui représente l'index de la tonalité choisie. Défaut: -1.
choixFormule = -1; // Variable globale qui représente l'index de la formule choisie. Défaut: -1.
choixMode = -1; // Variable globale qui représente l'index du mode choisi. Défaut: -1.
// stopNote = false; // 
let stopST = []; // Variable globale en tableau des numéros de référence définies par setTimeout().

stave = []; // Variable globale de portée musicale.

FONCTIONS_TONALES = ["I", "I6", "I64", "II6", "III", "IV", "IVm", "V", "V7", "V+6", "Vm", "V6aug/V"]; 
FONCTIONS_VOIX_MAJ = [
	[-12, 4, 7, 12], // Fonction I
	[-8, 0, 7, 12], // Fonction I6
	[-5, 4, 7, 12], // Fonction I64
	[-7, 5, 9, 14], // Fonction II6
	[-8, 4, 7, 11], // Fonction III
	[-7, 5, 9, 12], // Fonction IV
	[-7, 5, 8, 12], // Fonction iv
	[-5, 2, 7, 11], // Fonction V
	[-5, 5, 7, 11], // Fonction V7
	[-10, 5, 7, 11], // Fonction V+6
	[-5, 2, 7, 10], // Fonction v
	[-3, 0, 4, 9],  // Fonction VI
	[-4, 3, 6, 12] // Fonction V6aug/V
];
FONCTIONS_VOIX_MIN = [ 
	[0, 3, 7, 12], // Fonction i
	[-9, 0, 7, 12], // Fonction i6
	[-5, 3, 7, 12], // Fonction i64
	[-7, 5, 8, 14], // Fonction ii6
	[-8, 4, 7, 11], // Fonction III
	[-7, 5, 9, 12], // Fonction IV
	[-7, 5, 8, 12], // Fonction iv
	[-5, 2, 7, 11], // Fonction V
	[-5, 5, 7, 11], // Fonction V7
	[-10, 5, 7, 11], // Fonction V+6
	[-5, 2, 7, 10], // Fonction v
	[-4, 0, 3, 8],  // Fonction vi
	[-4, 3, 6, 12] // Fonction V6aug/V
];

gammes = [
	{"nom": "Dob majeur", "nalt": -7, "mode": 0, "os": 0, "midi": -1},
	{"nom": "Solb majeur", "nalt": -6, "mode": 0, "os": 4, "midi": 6},
	{"nom": "Réb majeur", "nalt": -5, "mode": 0, "os": 1, "midi": 1},
	{"nom": "Lab majeur", "nalt": -4, "mode": 0, "os": 5, "midi": 8},
	{"nom": "Mib majeur", "nalt": -3, "mode": 0, "os": 2, "midi": 3},
	{"nom": "Sib majeur", "nalt": -2, "mode": 0, "os": 6, "midi": 10},
	{"nom": "Fa majeur", "nalt": -1, "mode": 0, "os": 3, "midi": 5},
	{"nom": "Do majeur", "nalt": 0, "mode": 0, "os": 0, "midi": 0},
	{"nom": "Sol majeur", "nalt": 1, "mode": 0, "os": 4, "midi": 7},
	{"nom": "Ré majeur", "nalt": 2, "mode": 0, "os": 1, "midi": 2},
	{"nom": "La majeur", "nalt": 3, "mode": 0, "os": 5, "midi": 9},
	{"nom": "Mi majeur", "nalt": 4, "mode": 0, "os": 2, "midi": 4},
	{"nom": "Si majeur", "nalt": 5, "mode": 0, "os": 6, "midi": 11},
	{"nom": "Fa# majeur", "nalt": 6, "mode": 0, "os": 3, "midi": 6},
	{"nom": "Do# majeur", "nalt": 7, "mode": 0, "os": 0, "midi": 1},
	{"nom": "Lab mineur", "nalt": -7, "mode": 1, "os": 5, "midi": 8},
	{"nom": "Mib mineur", "nalt": -6, "mode": 1, "os": 2, "midi": 3},
	{"nom": "Sib mineur", "nalt": -5, "mode": 1, "os": 6, "midi": 10},
	{"nom": "Fa mineur", "nalt": -4, "mode": 1, "os": 3, "midi": 5},
	{"nom": "Do mineur", "nalt": -3, "mode": 1, "os": 0, "midi": 0},
	{"nom": "Sol mineur", "nalt": -2, "mode": 1, "os": 4, "midi": 7},
	{"nom": "Ré mineur", "nalt": -1, "mode": 1, "os": 1, "midi": 2},
	{"nom": "La mineur", "nalt": 0, "mode": 1, "os": 5, "midi": 9},
	{"nom": "Mi mineur", "nalt": 1, "mode": 1, "os": 2, "midi": 4},
	{"nom": "Si mineur", "nalt": 2, "mode": 1, "os": 6, "midi": 11},
	{"nom": "Fa# mineur", "nalt": 3, "mode": 1, "os": 3, "midi": 6},
	{"nom": "Do# mineur", "nalt": 4, "mode": 1, "os": 0, "midi": 1},
	{"nom": "Sol# mineur", "nalt": 5, "mode": 1, "os": 4, "midi": 8},
	{"nom": "Ré# mineur", "nalt": 6, "mode": 1, "os": 1, "midi": 3},
	{"nom": "La# mineur", "nalt": 7, "mode": 1, "os": 5, "midi": 10}];

//
// 									██████████████████████████████████
// 									██████████████████████████████████
// 									███████ GESTION DE L'AUDIO ███████
// 									██████████████████████████████████
// 									██████████████████████████████████

// ******************************************************************************************************
// jouerTonique(offset)
//		Joue une note.
// 		offset: offset MIDI.
jouerTonique = function(offset){
	playNotesNB([0], offset, 50, 100, 2, true);
}

// ******************************************************************************************************
// delayNB(note, vol, ms, on = true)
//		Délai non-bloquant pour la gestion du rythme.
// 		note: Note MIDI.
//		vol: Vélocité de 0 à 127.
//		ms: Valeur de délai en milliseconde.
//		on: Flag noteOn/noteOff. Défaut: true.
function delayNB(note, vol, ms, on = true) { // NVDO
	var startTime = Date.now();
	if (on) stopST.push(setTimeout(function(){midiSound.noteOn(note, vol);}, ms));
	else stopST.push(setTimeout(function(){midiSound.noteOff(note, vol);}, ms));
}


// ******************************************************************************************************
// playNotesNB(notes, offset, delai = DELAI_DEPART, tempo = 100, figureNote = 4, sustain = false)
//		Player non-bloquant d'une série de notes.
// 		notes: Tableau de notes MIDI.
//		offset: Valeur de transposition MIDI.
//		delai: Valeur en milliseconde avant lequel les notes seront jouées. Défaut: DELAI_DEPART.
//		tempo: Tempo en BPM. Défaut: 100.
//		figureNote: Figure de note en format dénominateur de fraction de ronde. 1 = Ronde, 2 = Blanche, 4 = Noire, etc. Défaut: 4. 
//		sustain: Flag de tenue de note. Défaut: false.
//		retour: Le délai ajusté pour les notes suivantes.
playNotesNB = function(notes, offset, delai = DELAI_DEPART, tempo = 100, figureNote = 4, sustain = false){ // NODTFS
	for (i = 0; i < notes.length; i++) {
		if (tempo > 0) {
			delayNB(MIDI_BASE + notes[i] + offset, 127, delai);
			delai += (60 / tempo) * (4 / figureNote) * 1000;
			if (!sustain) delayNB(MIDI_BASE + notes[i] + offset, 127, delai, false);
		} else {
			delayNB(MIDI_BASE + notes[i] + offset, 127, delai, false);
		}
	}
	return delai;
};

// ******************************************************************************************************
// playFonctionsTonalesNB(notes, offset, mode, delai = DELAI_DEPART, tempo = 100, figureNote = 4)
//		Player non-bloquant de fonctions tonales.
// 		notes: Tableau de fonctions tonales. Ex: ["I", "II6", "V7", "I"].
//		offset: Valeur de transposition MIDI.
//		mode: Mode majeur ou mineur.
//		delai: Valeur en milliseconde avant lequel les notes seront jouées. Défaut: DELAI_DEPART.
//		tempo: Tempo en BPM. Défaut: 100.
//		figureNote: Figure de note en format dénominateur de fraction de ronde. 1 = Ronde, 2 = Blanche, 4 = Noire, etc. Défaut: 4. 
//		retour: Le délai ajusté pour les notes suivantes.
playFonctionsTonalesNB = function(notes, offset, mode, delai = DELAI_DEPART, tempo = 100, figureNote = 4){ // NOMDTF
	let accord;
	for (var i = 0; i < notes.length; i++) {
		if (mode == 0) accord = FONCTIONS_VOIX_MAJ[FONCTIONS_TONALES.indexOf(notes[i])];
		else accord = FONCTIONS_VOIX_MIN[FONCTIONS_TONALES.indexOf(notes[i])];
		delai = playNotesNB(accord, offset, delai, 200, 64, true);
		if (i < notes.length - 1) delai += (60 / tempo) * (4 / figureNote) * 1000;
		else delai += (60 / tempo) * (8 / figureNote) * 1000;
		delai = playNotesNB(accord, offset, delai, 0);
	}
	return delai;
};

// ******************************************************************************************************
// stopTimer()
//		Fonction qui interrompt tous les setTimeout().
stopTimer = function(){
	while(stopST.length > 0) clearTimeout(stopST.pop());
}

// ******************************************************************************************************
// resetButton(id, textContent, delai)
//		Fonction qui modifie le texte d'un bouton après un certain délai.
//		id: l'identifiant du bouton.
//		textContent: Le nouveau texte que doit prendre le bouton.
//		delai: Le délai après lequel le texte doit changer.
resetButton = function(id, textContent, delai){
	stopST.push(setTimeout(function(){document.getElementById(id).textContent = textContent;}, delai));
}

// ******************************************************************************************************
// installationTonalite(offset, mode)
//		Installation de la tonalité à la manière de Luce.
//		offset: Valeur de transposition MIDI.
//		mode: Mode majeur ou mineur.
installationTonalite = function(offset, mode){
	var tempo = 100, delai = DELAI_DEPART;

	// for (i = 0; i < 10; i++) {
	// 	testST(i * 1000);
	// 	console.log("installationTonalite:", i);
	// }
	resetButton("installerTonalite", "Stop Installation", delai);

	delai = playNotesNB([0, 2, 4 - mode, 5, 7, 9, 11, 12], offset, delai, tempo, 16);
	delai = playNotesNB([11 - mode, 9 - mode, 7, 5, 4 - mode, 2], offset, delai, tempo, 16);
	delai = playNotesNB([0], offset, delai, tempo, 4);
	delai = playNotesNB([-5, -1, 2, 5, 4 - mode, 2], offset, delai, tempo - 10, 16);
	delai = playNotesNB([0], offset, delai, tempo, 4);

	delai = playFonctionsTonalesNB(["I", "II6", "V7", "I"], offset, mode, delai, tempo);
	resetButton("installerTonalite", "Installer la tonalité", delai);
};

//
// 									███████████████████████████████████
// 									███████████████████████████████████
// 									███████ GESTION DES BOUTONS ███████
// 									███████████████████████████████████
// 									███████████████████████████████████

// ******************************************************************************************************
// genererFormule()
//		Génération aléatoire d'une formule.
genererFormule  = function(){
	var formuleDegre, ancienneFormule;
	ancienneFormule = choixFormule;
	do {
		choixFormule = Math.floor(Math.random() * formules[choixMode].length);
	} while (ancienneFormule == choixFormule);
	gestionDropDown();
};

// ******************************************************************************************************
// genererTonalite()
//		Génération aléatoire d'une tonalité et d'un mode.
genererTonalite  = function(){
	choixTonalite = Math.floor(Math.random() * gammes.length);
	choixMode = gammes[choixTonalite].mode;
	gestionDropDown();
};

// ******************************************************************************************************
// prochaineTonalite(inverser)
//		Incrémentation ou décrémentation de la position dans le tableau des tonalités.
// 		inverser: true pour décrémenter.
prochaineTonalite = function(inverser){
	if (inverser)
		if (choixTonalite > 0) choixTonalite--; 
		else choixTonalite = gammes.length - 1;
	else
		if (choixTonalite < gammes.length - 1) choixTonalite++; 
		else choixTonalite = 0;
	choixMode = gammes[choixTonalite].mode;
	gestionDropDown();
};

// ******************************************************************************************************
// prochaineFormule(inverser)
//		Incrémentation ou décrémentation de la position dans le tableau des formules.
// 		inverser: true pour décrémenter.
prochaineFormule = function(inverser){
	if (inverser)
		if (choixFormule > 0) choixFormule--; 
		else choixFormule = formules[choixMode].length - 1;
	else
		if (choixFormule < formules[choixMode].length - 1) choixFormule++; 
		else choixFormule = 0;
	gestionDropDown();
};

findIndexGamme = function(element, index, array){
	return element.nom == this.tempTonalite + " " + this.tempMode.toLowerCase();
};

// ******************************************************************************************************
// trouveGamme(chaine)
//		Suite de notes latines à afficher sous forme de chaine.
//		retour: l'index de la gamme.
trouveGamme = function(){
	var tempTonalite = Session.get("objetTonalite");
	var tempMode = Session.get("objetMode");
	var temp = gammes.findIndex(findIndexGamme, {tempTonalite, tempMode});
	if (temp == -1) {
		tempMode = DROPDOWN_MODE[1 - DROPDOWN_MODE.indexOf(tempMode)];
		temp = gammes.findIndex(findIndexGamme, {tempTonalite, tempMode});
		Session.set("objetMode", tempMode);
	}
	return temp;
}

// ******************************************************************************************************
gestionDropDown = function(event = null){
	if (event != null) {
		switch (event.currentTarget.id){
			case "choixTonalite": Session.set("objetTonalite", $(event.currentTarget).text()); break;
			case "choixMode": Session.set("objetMode", $(event.currentTarget).text()); break;
			case "choixFormule": Session.set("objetFormule", $(event.currentTarget).text()); break;
		}
	    choixTonalite = trouveGamme();
    	choixMode = gammes[choixTonalite].mode;
		if (choixMode == 0) choixFormule = DROPDOWN_FORMULES_MAJ.indexOf(Session.get("objetFormule"));
		else choixFormule = DROPDOWN_FORMULES_MIN.indexOf(Session.get("objetFormule"));
	} else {
		Session.set("objetTonalite", gammes[choixTonalite].nom.split(' ')[0].slice());
		Session.set("objetMode", DROPDOWN_MODE[choixMode]);
		if (choixMode == 0) Session.set("objetFormule", DROPDOWN_FORMULES_MAJ[choixFormule]);
		else Session.set("objetFormule", DROPDOWN_FORMULES_MIN[choixFormule]);
		Session.set("htmlReponse", "");
	}
	reinitialisationMesure();
	reinitialiserInterface();
}

// ******************************************************************************************************
reinitialiserInterface = function(){
	stopTimer();
	resetButton("installerTonalite", "Installer la tonalité", 0);
	resetButton("demarrerSolfege", "Démarrer accompagnement", 0);
}

// ******************************************************************************************************
afficherEnsembleFormules = function(){
	nouvelle = gammes[choixTonalite];
	document.getElementById("afficherFormules").textContent = "Cacher partition";
	afficherMesure(convertisseurAbc(transposeAbc("c", nouvelle), CVT_VEX), "1");
	for (var i = 0; i < formules[choixMode].length; i++) {
		if (choixMode == 0) 
			afficherMesure(convertisseurAbc(transposeAbc(formules[choixMode][i], nouvelle), CVT_VEX), DROPDOWN_FORMULES_MAJ[i], 0);	
		else
			afficherMesure(convertisseurAbc(transposeAbc(formules[choixMode][i], nouvelle), CVT_VEX), DROPDOWN_FORMULES_MIN[i], 0);
	}
};

//
// 									███████████████████████████████████
// 									███████████████████████████████████
// 									███████ GESTION DES DONNÉES ███████
// 									███████████████████████████████████
// 									███████████████████████████████████
//

// ******************************************************************************************************
// formaterLatin(chaine)
//		Suite de notes latines à afficher sous forme de chaine.
// 		tableau: tableaui en abc à afficher.
formaterLatin = function(tableau){
	var retour = "";	// Variable de retour.
	for (i = 0; i < tableau.length - 1; i++) { // PArcours du tableau.
		retour += tableau[i] + ", "; // Ajout d'un élément suivi d'une virgule.
	}
	retour += tableau[i] + ". "; // Ajout d'un élément suivi d'un point.
	return retour;
};

// ******************************************************************************************************
// transposeAbc(chaine, nouvelleGamme)
// 		chaine: chaine en abc à transposer. Ex: "GABc", "G=A=Bc_B_AG"
//		nouvelleGamme: objet gamme.
//		return: chaine transposée. Ex: "DEFG" en Sol.
transposeAbc = function(chaine, nouvelleGamme){
	var retour = "";		// Variable de retour.
	var chaineDegre;		// Chaine contenant la formule convertie en degrés.
	var octave;				// Registre de la note.
	var pushAlt = false;	// Flag de concaténation de l'altération à la chaine convertie.
	var posAbsAlt = 0;		// Position absolue de l'altération dans le tableau ABC_ALT_TAB.
	var posRelAlt = 0;		// Position relative dans le tableau ABC_ALT_TAB.
	var alt = "";			// Partie de la chaine correspondant à l'altération seulement.
	var suiteAlterations;	// Suite des degrés altérés d'une tonalité. 

	chaineDegre = abc2degre(chaine); // Conversion de la formule en degrés.
	suiteAlterations = genererSuiteAlterations(nouvelleGamme); // Sélection de la bonne suite.
	if (nouvelleGamme.os < SEUIL_OCTAVE) {console.log("0");octave = 0;} else octave = SAUT_OCTAVE; // Ajustement des gammes aigus.

	for (i = 0, k = 0; i < chaine.length; i++){ // Parcours de la formule abc contenue dans chaine.
		switch (ABC_ALT_TAB.indexOf(chaine[i])) { // Détection si le caractère est une altération.
			case -1: // Si c'est une note, ajout de l'altération (si présente) et de la note.
				for (j = 0; j < Math.abs(nouvelleGamme.nalt); j++) // Parcours de la suite d'altérations.
					if (chaineDegre[k] == suiteAlterations[j]) { // Altération trouvée dans la suite.
						pushAlt = true; // Prêt pour la concaténation.
						if (nouvelleGamme.nalt < 0) posAbsAlt = -1; else posAbsAlt = 1; // Type d'altération.
						break;
					}
				if (pushAlt) // Récupération de l'altération à afficher.
					alt = ABC_ALT_TAB[posAbsAlt + 2 + posRelAlt]; 
				retour += alt + SUITE_NOTES[SUITE_NOTES.indexOf(chaine[i]) - octave + nouvelleGamme.os]; // Concaténation.
				alt = ""; pushAlt = false; k++; posAbsAlt = 0; posRelAlt = 0; // Réinitialisation.
				break;
			case 1: break; // Cas "_": Rien car déjà géré par suiteAltérations en mineur.
			case 2: case 3: // Cas "=" et "^": Altération doit être explicitée et dièsée de un.
				posRelAlt = 1; pushAlt = true; 
				break; 
		}
	}
    // console.log("(transposeAbc) [" + chaine + "]", "->", retour + ", FormuleDegre: " + chaineDegre + ".");
	return retour;
};

// ******************************************************************************************************
// genererSuiteAlterations(nouvelleGamme)
//		Génération d'une suite de degrés altérés en fonction du mode et du type de tonalité.
// 		nouvelleGamme: gamme à analyser.
//		retour: la suite de degrés altérés.
genererSuiteAlterations = function(nouvelleGamme){
	if (nouvelleGamme.mode == 0) retour = SUITE_ALT_MAJ.slice(); // Mode majeur.
	else retour = SUITE_ALT_MIN.slice(); // Mode mineur.
	if (nouvelleGamme.nalt < 0) retour.reverse(); // Inversion pour les tonalités bémolisées.
	return retour;
};

// ******************************************************************************************************
// convertisseurAbc(chaine, option)
// 		chaine: chaine en abc à convertir soit en nom latin ou en Vex.Flow. Ex: "GABc"
//		option: 0 = Latin, 1 = VexFlow.
//		retour: chaine convertie. Ex: "Sol", "La", "Si", "Do".
convertisseurAbc = function(chaine, option){
	var retour = [];		// Variable de retour.
	var octave = REGISTRE;	// Registre de la note.
	var push = false;		// Flag de concaténation à la chaine convertie.
	var alt = "";			// Partie de la chaine correspondant à l'altération seulement.
	var note = "";			// Partie de la chaine correspondant à la note seulement.
	var altMidi = 0;		// Ajustement de la note midi en fonction de l'altération.

	for (i = 0; i < chaine.length; i++) { // Parcours de la chaine à convertir.
		switch (true) {
			case ABC_ALT.includes(chaine[i]): // Cas des altérations abc. Pas de push.
				alt += ABC2VEX_ALT[chaine[i]]; // Conversion d'une altération abc en VEX.
				altMidi += ABC_ALT.indexOf(chaine[i]) - 1; // Ajustement midi. -1 pour bémol, +1 pour dièse.
				break;
			case chaine[i] == ",": break; // Cas du registre grave. Voir cas d'une note registe moyen.
			case chaine[i] == chaine[i].toLowerCase(): // Cas d'une note registre aigu. Push note.
				octave = REGISTRE + 1; // Octave au dessus. Pas de break pour éviter doublons.
			case chaine[i] == chaine[i].toUpperCase(): // Cas d'une note registe moyen. Push note.
				if (chaine[i + 1] == ",") octave = REGISTRE - 1; // Octave en dessous.
				note = chaine[i]; // Récupération de la note.
				push = true; // Chaine prête à la concaténation.
				break;
		}
		if (push) { // Opérations de concaténation.
			switch (option) {
				case CVT_LATIN: // Conversion en notes latines. 
					if (alt == "n") alt = ""; // Aucun caractère lorsque bécarre.
					retour.push(ABC2LAT_NOTES[note.toUpperCase()] + alt); // Concaténation.
					break;
				case CVT_VEX: // Conversion en notes Vex.
					retour.push(note.toUpperCase() + alt + "/" + octave); // Concaténation en Vex. 
					break;
				case CVT_MIDI:
					retour.push(MIDI_DOMAJ[MIDI_NOTES.indexOf(note.toUpperCase())] + ((octave - REGISTRE) * 12) + altMidi);
					break;
			}
			push = false; // Réinitialisation.
			octave = REGISTRE;
			alt = "";
			altMidi = 0;
		}
	}
    // console.log("convertisseurAbc(" + option + "): ", retour);
	return retour;
};

// ******************************************************************************************************
// abc2degre(chaine)
//		Converrsion d'une chaine en format abc à une chaine en degrés musicaux.
// 		chaine: chaine en abc à convertir. Ex: "GABc"
//		retour: chaine convertie en degrés. Ex: "5671"
abc2degre = function(chaine){
	var retour = "", j;

	for (i = 0; i < chaine.length; i++) { // Parcours de la chaine à convertir.
		if (!ABC_ALT.includes(chaine[i])) { // Test pour exclure les altérations de la conversion.
			j = SUITE_NOTES.indexOf(chaine[i]); // Récupération de l'index corespondant à la note.
			j = (j % SAUT_OCTAVE) + 1; // Retrait de la notion de registre de la note.
			retour += j; // Construction de la chaine convertie.
		}
	}
	return retour;
};

//
// 									████████████████████████████████████
// 									████████████████████████████████████
// 									██████ GESTION DE L'AFFICHAGE ██████
// 									████████████████████████████████████
// 									████████████████████████████████████
//

// ******************************************************************************************************
// reinitialisationMesure()
//		Réinitialisation à zéro des portées et de l'affichage. 
reinitialisationMesure = function(){
	document.getElementById("afficherFormules").textContent = "Afficher partition";
	Session.set("htmlReponse", "");
    if(typeof ctx != 'undefined') // Effacer le canvas d'affichage.
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	stave = []; // Réinitialisation à zéro de la portée.
};


// ******************************************************************************************************
// afficherMesure(notes, tabChaine, efface)
//		Affichage d'une mesure avec des notes et du texte.
//		notes: tableau de notes en format Vex à afficher.
//		tabChaine: tableau de chaine de caractères à afficher au dessus des notes.
//		efface: true: efface le canvas avant d'afficher.
afficherMesure = function(notes, tabChaine, efface = false) {
	var stavesWidth = VEX_STAVES_WIDTH; // Largeur de mesure.

	// if (efface) reinitialisationMesure();
	if (notes.length > 4) // Allongement de la largeur de mesure si trop de notes.
		stavesWidth *= VEX_LARGE_STAVES_WIDTH_RATIO; 
	if (notes.length < 2)
		stavesWidth = Math.round(stavesWidth / VEX_LARGE_STAVES_WIDTH_RATIO, 0);
	
	if(stave.length == 0){ // Création de la première mesure.
		var tempStave = new Vex.Flow.Stave(10, 0, stavesWidth); 
		tempStave.addClef("treble").setContext(ctx).draw();
   	}else // Création des mesures ajoutées à la suite de la première.
   		var tempStave = new Vex.Flow.Stave(stave[stave.length-1].width + stave[stave.length-1].x, stave[stave.length-1].y, stavesWidth);
  	tempStave.setEndBarType(Vex.Flow.Barline.type.DOUBLE);

    // Gestion des notes
    var notesBar1 = [];
    for(var i = 0; i < notes.length; i++){
    	alteration = notes[i].split( '/')[0].slice(1);
    	if (alteration == "")
    		notesBar1.push(new Vex.Flow.StaveNote({keys: [notes[i]], duration: VEX_NOTE_DURATION }));
    	else
    		notesBar1.push(new Vex.Flow.StaveNote({keys: [notes[i]], duration: VEX_NOTE_DURATION }).addAccidental(0, new Vex.Flow.Accidental(alteration)));
    }
    // Gestion du texte
    // for (i = 0; i < tabChaine.length; i++) {
    	var text = new Vex.Flow.TextNote({
			text: tabChaine,
			font: {
	    		family: "Arial",
	    		size: 12,
	    		weight: ""
			},
			duration: 'q'
    	})
	    	.setLine(2)
	    	.setStave(tempStave);
	    	// .setJustification(Vex.Flow.TextNote.Justification.LEFT);
   	    text.setTickContext(new Vex.Flow.TickContext().setX(0));
    // }

    // Réajustement de la position dela mesure en fonction de la largeur de la fenêtre.
    if(tempStave.width + tempStave.x + 25 > window.innerWidth ) {
    	tempStave.setX(10);
    	tempStave.setY(tempStave.y + VEX_SPACE_BETWEEN_STAVES);
   		tempStave.addClef("treble").setContext(ctx).draw();
    }
	stave.push(tempStave); // Ajout de la nouvelle mesure à la portée.
   	tempStave.setContext(ctx).draw(); // Connect the stave to the rendering context and draw!
    text.setContext(ctx).draw(); // Connect the text to the rendering context and draw!
    Vex.Flow.Formatter.FormatAndDraw(ctx, tempStave, notesBar1); // Format and draw the stave.
};

/*
// ******************************************************************************************************
function delayB(ms) {
	var startTime = Date.now();
	while(Date.now() - startTime < ms);
}

// ******************************************************************************************************
playNotesB = function(notes, offset, tempo = 100, figureNote = 4, sustain = false){ // NOTFS
	var sampleCount = 4 / figureNote;
	var delai = (60 / tempo) * (4 / figureNote) * 1000;
	for (i = 0; i < notes.length; i++) {
		if (tempo > 0) {
			midiSound.noteOn(60 + notes[i] + offset, 127);
			delayB(delai);
			if (!sustain) midiSound.noteOff(60 + notes[i] + offset, 127);
		} else {
			midiSound.noteOff(60 + notes[i] + offset, 127);
		}
	}
};

playFonctionsTonalesB = function(fonctions, offset, mode, tempo = 100, figureNote = 4){ // FOMTF
	let accord;
	var delai = (60 / tempo) * (4 / figureNote) * 1000;
	for (var i = 0; i < fonctions.length; i++) {
		if (mode == 0) accord = FONCTIONS_VOIX_MAJ[FONCTIONS_TONALES.indexOf(fonctions[i])];
		else accord = FONCTIONS_VOIX_MIN[FONCTIONS_TONALES.indexOf(fonctions[i])];
		playNotesB(accord, offset, 200, 64, true);
		if (i < fonctions.length - 1) delayB(delai); else delayB(delai * 2);
		playNotesB(accord, offset, 0, 200);
	}	
};

playAccords = function(accord, offset, figureNote, tempo){
	var accordTranspose;
	var sampleCount = (4 / figureNote) / 1.5;
	var delai = (60 / tempo) * (4 / figureNote) * 1000;
	// debugger
	for (var i = 0; i < accord.length; i++) {
		accordTranspose = accord[i].slice();
		playNotes(accordTranspose, offset, 64, 100);
		delay(delai);
	}	
}


// ******************************************************************************************************
testST = function(ms){
	var temp = setTimeout(function(){console.log("testST:", ms);}, ms);
	stopST.push(temp);
}
*/