import { Template } from 'meteor/templating';
import './ex3.html';

// ******************************************************************************************************
function afficherReponse(){
	var formule;
	// debugger
	reinitialisationMesure();
	document.getElementById("afficherFormules").textContent = "Cacher partition";
	formule = formules[choixMode][choixFormule];
	formuleTransposee = transposeAbc(formule, gammes[choixTonalite])
	// tabLat = convertisseurAbc(formuleTransposee, CVT_LATIN);
	tabVex = convertisseurAbc(formuleTransposee, CVT_VEX);
	// Session.set("htmlReponse", formaterLatin(tabLat));
	afficherMesure(tabVex, "");
};

// ******************************************************************************************************
function jouerDegre(){
	var temp = convertisseurAbc(transposeAbc(popDegre(formules[choixMode][choixFormule]), gammes[choixTonalite]), CVT_MIDI);
	jouerTonique(temp[0]);
}

// ******************************************************************************************************
function jouerReponse(offset, formule){
	var notes = convertisseurAbc(formule, CVT_MIDI), delai = DELAI_DEPART;
	resetButton("jouerReponse", "Stop réponse", delai);
	console.log(notes);
	delai = playNotesNB(notes, offset, delai);
	resetButton("jouerReponse", "Jouer la réponse", delai);
}

// ******************************************************************************************************
function popDegre(formule){
	var retour;
	if (ABC_ALT.includes(formule[0])) retour = formule[0] + formule[1];
	else retour = formule[0];
	console.log(retour);
	return retour;
}

// ******************************************************************************************************
function afficherDegreDepart(){
	var formuleTransposee;

	reinitialisationMesure();
	// debugger
	degre = popDegre(formules[choixMode][choixFormule]);
	formuleTransposee = transposeAbc(degre, gammes[choixTonalite])
	// tabLat = convertisseurAbc(formuleTransposee, CVT_LATIN);
	tabVex = convertisseurAbc(formuleTransposee, CVT_VEX);
	// Session.set("htmlReponse", formaterLatin(tabLat));
	afficherMesure(tabVex, "");
};


// ******************************************************************************************************
function gestionAuto(){
	if ($('#autoDegre').is(':checked')) jouerDegre();
	// if ($('#autoReponse').is(':checked')) afficherReponse();
	// if ($('#autoInstallation').is(':checked')) installationTonalite(gammes[choixTonalite].midi, choixMode);
	// afficherDegreDepart();
}

// ******************************************************************************************************
Template.exercice3.events({
	'click #afficherFormules': function (e) {
		if (document.getElementById("afficherFormules").textContent == "Afficher partition") {
		afficherReponse();
		} else reinitialisationMesure();
	},
	'click #jouerTonique': function (e) {
		// debugger
		var temp = convertisseurAbc(transposeAbc(popDegre(formules[choixMode][choixFormule]), gammes[choixTonalite]), CVT_MIDI);
		jouerTonique(temp[0]);
	},
	'click #jouerReponse': function (e) {
		reinitialiserInterface();
		if (document.getElementById("jouerReponse").textContent == "Jouer la réponse") {
			jouerReponse(gammes[choixTonalite].midi - 12, formules[choixMode][choixFormule]);
		} else document.getElementById("jouerReponse").textContent = "Jouer la réponse";
	},
	'click #genererFormule': function (e) {genererFormule();gestionAuto();},
	'click #genererTonalite': function (e) {genererTonalite();gestionAuto();},
	'click #genererTonForm': function (e) {genererTonalite();genererFormule();gestionAuto();},
	'click #tonalitePrecedente': function(e) {prochaineTonalite(true);gestionAuto();},
	'click #tonaliteSuivante': function(e) {prochaineTonalite();gestionAuto();},
	'click #formulePrecedente': function(e) {prochaineFormule(true);gestionAuto();},
	'click #formuleSuivante': function(e) {prochaineFormule();gestionAuto();},
	'click #choixTonalite': function(event) {gestionDropDown(event);gestionAuto();},
	'click #choixMode': function(event) {gestionDropDown(event);gestionAuto();},
	'click #choixFormule': function(event) {gestionDropDown(event);gestionAuto();},
	'click #autoDegre': function(event) {
		if(  $('#autoDegre').is(':checked')  ) {
			stopTimer();
			jouerDegre();
		}
	}
});

// ******************************************************************************************************
Template.exercice3.helpers({
	htmlFormule:function(){
		if (Session.get("objetFormule") == undefined) {
			Session.set("objetMode", "Majeur");
			Session.set("objetTonalite", "Do");
			Session.set("objetFormule", "21");
			choixMode = 0;
			choixTonalite = 7;
			choixFormule = 0;
		}
	    return Session.get("objetFormule");
	},
	htmlTonalite:function(){
	    return Session.get("objetTonalite") + " " + Session.get("objetMode");
	},
	tonalitesDropdown:function(){
	    return DROPDOWN_TONALITE;
	},
	modeDropdown:function(){
	    return DROPDOWN_MODE;
	},
	formuleDropdown:function(){	// Affichage des choix disponible dans le dropdown.
		if (choixMode == 0) return DROPDOWN_FORMULES_MAJ;
		else return DROPDOWN_FORMULES_MIN;
	},
	mode:function(){
		if (Session.get("objetMode") == undefined) {
			Session.set("objetMode", "Majeur");
			choixMode = 0;
		}
	    return Session.get("objetMode");
	},
	tonalite:function(){
		if (Session.get("objetTonalite") == undefined) {
			Session.set("objetTonalite", "Do");
			choixTonalite = 7;
		}
	    return Session.get("objetTonalite");
	},
	formule:function(){	// Affichage du dropdown à l'état normal.
		if (Session.get("objetFormule") == undefined) {
			Session.set("objetFormule", "21");
			choixFormule = 0;
		}
		// setTimeout(function(){afficherDegreDepart();}, 100);
		return Session.get("objetFormule");
	}
});

// ******************************************************************************************************
Template.exercice3.onRendered(function() {
	canvas    = document.getElementById("score");
   // Get the rendering context
  	renderer = new Vex.Flow.Renderer(canvas, Vex.Flow.Renderer.Backends.CANVAS);
  	ctx = renderer.getContext(); 
});