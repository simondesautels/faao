import { Template } from 'meteor/templating';
import './ex2.html';

// ******************************************************************************************************
var afficherReponse = function(){
	var formule;
	// debugger
	reinitialisationMesure();
	document.getElementById("afficherFormules").textContent = "Cacher partition";
	formule = formules[choixMode][choixFormule];
	formuleTransposee = transposeAbc(formule, gammes[choixTonalite])
	tabLat = convertisseurAbc(formuleTransposee, CVT_LATIN);
	tabVex = convertisseurAbc(formuleTransposee, CVT_VEX);
	Session.set("htmlReponse", formaterLatin(tabLat));
	afficherMesure(tabVex, "");
};

// ******************************************************************************************************
var jouerReponse = function(offset, formule){
	// debugger
	var notes = convertisseurAbc(formule, CVT_MIDI), delai = DELAI_DEPART;
	resetButton("jouerReponse", "Stop réponse", delai);
	console.log(notes);
	delai = playNotesNB(notes, offset, delai);
	resetButton("jouerReponse", "Jouer la réponse", delai);
}

// ******************************************************************************************************
function gestionAuto(){
	if ($('#autoReponse').is(':checked')) afficherReponse();
	if ($('#autoInstallation').is(':checked')) installationTonalite(gammes[choixTonalite].midi, choixMode);
}

// ******************************************************************************************************
Template.exercice2.events({
	'click #afficherFormules': function (e) {
		if (document.getElementById("afficherFormules").textContent == "Afficher partition") {
		afficherReponse();
		} else reinitialisationMesure();
	},
	'click #installerTonalite': function (e) {
		reinitialiserInterface();
		if (document.getElementById("installerTonalite").textContent == "Installer la tonalité") {
			installationTonalite(gammes[choixTonalite].midi, choixMode);
		} else document.getElementById("installerTonalite").textContent = "Installer la tonalité";
	},
	'click #jouerTonique': function (e) {jouerTonique(gammes[choixTonalite].midi);},
	'click #jouerReponse': function (e) {
		reinitialiserInterface();
		if (document.getElementById("jouerReponse").textContent == "Jouer la réponse") {
			jouerReponse(gammes[choixTonalite].midi - 12, formules[choixMode][choixFormule]);
		} else document.getElementById("jouerReponse").textContent = "Jouer la réponse";
	},
	'click #genererFormule': function (e) {genererFormule();gestionAuto();},
	'click #genererTonalite': function (e) {genererTonalite();gestionAuto();},
	'click #tonalitePrecedente': function(e) {prochaineTonalite(true);gestionAuto();},
	'click #tonaliteSuivante': function(e) {prochaineTonalite();gestionAuto();},
	'click #formulePrecedente': function(e) {prochaineFormule(true);gestionAuto();},
	'click #formuleSuivante': function(e) {prochaineFormule();gestionAuto();},
	'click #choixTonalite': function(event) {gestionDropDown(event);gestionAuto();},
	'click #choixMode': function(event) {gestionDropDown(event);gestionAuto();},
	'click #choixFormule': function(event) {gestionDropDown(event);gestionAuto();},
	'click #autoInstallation': function(event) {
		if(  $('#autoInstallation').is(':checked')  ) {
			reinitialiserInterface();
			installationTonalite(gammes[choixTonalite].midi, choixMode);
		}
	},
	'click #autoReponse': function(event) {
		reinitialisationMesure();
		if(  $('#autoReponse').is(':checked')  ) {
			document.getElementById("afficherFormules").disabled = true;
			afficherReponse();
		} else 
			document.getElementById("afficherFormules").disabled = false;
	}
});

// ******************************************************************************************************
Template.exercice2.helpers({
	htmlFormule:function(){
	    return Session.get("htmlFormule");
	},
	htmlTonalite:function(){
	    return Session.get("htmlTonalite");
	},
	htmlReponse:function(){
	    return Session.get("htmlReponse");
	},
	tonalitesDropdown:function(){
	    return DROPDOWN_TONALITE;
	},
	modeDropdown:function(){
	    return DROPDOWN_MODE;
	},
	formuleDropdown:function(){
		if (choixMode == 0) return DROPDOWN_FORMULES_MAJ;
		else return DROPDOWN_FORMULES_MIN;
	},
	mode:function(){
		if (Session.get("objetMode") == undefined) {
			Session.set("objetMode", "Majeur");
			choixMode = 0;
		}
	    return Session.get("objetMode");
	},
	tonalite:function(){
		if (Session.get("objetTonalite") == undefined) {
			Session.set("objetTonalite", "Do");
			choixTonalite = 7;
		}
	    return Session.get("objetTonalite");
	},
	formule:function(){
		if (Session.get("objetFormule") == undefined) {
			Session.set("objetFormule", "21");
			choixFormule = 0;
		}
		return Session.get("objetFormule");
	}
});

// ******************************************************************************************************
Template.exercice2.onRendered(function() {
	canvas = document.getElementById("score");
   // Get the rendering context
  	renderer = new Vex.Flow.Renderer(canvas, Vex.Flow.Renderer.Backends.CANVAS);
  	ctx = renderer.getContext(); 
});
