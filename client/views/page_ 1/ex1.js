import { Template } from 'meteor/templating';
import './ex1.html';

// ******************************************************************************************************
function demarrerSolfegePiano(offset, mode){
	var tempo = 100, delai = DELAI_DEPART;

	resetButton("demarrerSolfege", "Stop accompagnement", delai);
	delai = playFonctionsTonalesNB(["I"], offset, mode, delai, tempo); // 1
	delai = playFonctionsTonalesNB(["V7", "I"], offset, mode, delai, tempo); // 21
	delai = playFonctionsTonalesNB(["I6", "V+6", "I"], offset, mode, delai, tempo); // 321
	delai = playFonctionsTonalesNB(["V", "I", "V7", "I"], offset, mode, delai, tempo); // 5151
	delai = playFonctionsTonalesNB(["II6", "I64", "V", "I"], offset, mode, delai, tempo); // 4321

	if (mode == 0) {
		delai = playFonctionsTonalesNB(["I6", "II6", "V7", "I"], offset, mode, delai, tempo); // 5671
		delai = playFonctionsTonalesNB(["II6", "V7", "I"], offset, mode, delai, tempo); // 671
		delai = playFonctionsTonalesNB(["IV", "I"], offset, mode, delai, tempo); // 61
		delai = playFonctionsTonalesNB(["V7", "I"], offset, mode, delai, tempo); // 71
	} else {
		delai = playFonctionsTonalesNB(["V6aug/V", "V7", "I"], offset, mode, delai, tempo); // 6-51
		delai = playFonctionsTonalesNB(["V6aug/V", "V7", "I"], offset, mode, delai, tempo); // 6-51
		delai = playFonctionsTonalesNB(["IVm", "I"], offset, mode, delai, tempo); // 6-1
		delai = playFonctionsTonalesNB(["IV", "I"], offset, mode, delai, tempo); // 61
		delai = playFonctionsTonalesNB(["Vm", "I"], offset, mode, delai, tempo); // 7-1
		delai = playFonctionsTonalesNB(["V7", "I"], offset, mode, delai, tempo); // 71
		delai = playFonctionsTonalesNB(["IVm", "Vm", "I"], offset, mode, delai, tempo); // 6-7-1
		delai = playFonctionsTonalesNB(["IVm", "V7", "I"], offset, mode, delai, tempo); // 6-71
		delai = playFonctionsTonalesNB(["IV", "V7", "I", "Vm", "V6aug/V", "V"], offset, mode, delai, tempo); // 5671765
	}
	resetButton("demarrerSolfege", "Démarrer accompagnement", delai);
};

// ******************************************************************************************************
function gestionAuto(){
	if ($('#autoFormules').is(':checked')) afficherEnsembleFormules();
	if ($('#autoInstallation').is(':checked')) installationTonalite(gammes[choixTonalite].midi, choixMode);
}

// ******************************************************************************************************
Template.exercice1.events({
	'click #afficherFormules': function (e) {
		if (document.getElementById("afficherFormules").textContent == "Afficher partition") {
		afficherEnsembleFormules();
		} else reinitialisationMesure();
	},
	'click #installerTonalite': function (e) {
		reinitialiserInterface();
		if (document.getElementById("installerTonalite").textContent == "Installer la tonalité") {
			installationTonalite(gammes[choixTonalite].midi, choixMode);
		} else document.getElementById("installerTonalite").textContent = "Installer la tonalité";
	},
	'click #demarrerSolfege': function (e) {
		reinitialiserInterface();
		if (document.getElementById("demarrerSolfege").textContent == "Démarrer accompagnement") {
			demarrerSolfegePiano(gammes[choixTonalite].midi, choixMode);
		} else document.getElementById("demarrerSolfege").textContent = "Démarrer accompagnement";
	},
	'click #genererTonalite': function (e) {genererTonalite();gestionAuto();},
	'click #tonalitePrecedente': function(e) {prochaineTonalite(true);gestionAuto();},
	'click #tonaliteSuivante': function(e) {prochaineTonalite();gestionAuto();},
	'click #choixTonalite': function(event) {gestionDropDown(event);gestionAuto();},
	'click #choixMode': function(event) {gestionDropDown(event);gestionAuto();},
	'click #autoInstallation': function(event) {
		if(  $('#autoInstallation').is(':checked')  ) {
			reinitialiserInterface();
			installationTonalite(gammes[choixTonalite].midi, choixMode);
		} 
	},
	'click #autoFormules': function(event) {
		reinitialisationMesure();
		if(  $('#autoFormules').is(':checked')  ) {
			document.getElementById("afficherFormules").disabled = true;
			afficherEnsembleFormules();
		} else 
			document.getElementById("afficherFormules").disabled = false;
	}
});

// ******************************************************************************************************
Template.exercice1.helpers({
	tonalitesDropdown:function(){
	    return DROPDOWN_TONALITE;
	},
	modeDropdown:function(){
	    return DROPDOWN_MODE;
	},
	tonalite:function(){
		if (Session.get("objetTonalite") == undefined) {
			Session.set("objetTonalite", "Do");
			choixTonalite = 7;
		}
	    return Session.get("objetTonalite");
	},
	mode:function(){
		if (Session.get("objetMode") == undefined) {
			Session.set("objetMode", "Majeur");
			choixMode = 0;
		}
	    return Session.get("objetMode");
	}
});

// ******************************************************************************************************
Template.exercice1.onRendered(function() {
	canvas    = document.getElementById("score");
   // Get the rendering context
  	renderer = new Vex.Flow.Renderer(canvas, Vex.Flow.Renderer.Backends.CANVAS);
  	ctx = renderer.getContext(); 
});


// ******************************************************************************************************
// function demarrerSolfegeGuitare(offset, mode){
// 	var tempo = 100;

// 	playStrumChord([[8,-1,5,5,5 - mode,8]], offset, 2, tempo);	// Formule 1

// 	playStrumChord([[-1,5,5,7,6,7]], offset, 4, tempo);	// Formule 21
// 	playStrumChord([[8,-1,5,5,5 - mode,8]], offset, 2, tempo);

// 	playStrumChord([[-1,7 - mode,5,5,5 - mode,8], [-1,5,5,7,6,7]], offset, 4, tempo);	// Formule 321
// 	playStrumChord([[8,-1,5,5,5 - mode,8]], offset, 2, tempo);

// 	playStrumChord([[-1,8,7 - mode,10,10 - mode,10], [-1,7 - mode,5,5,5 - mode,8], [-1,5,5,7,6,7]], offset, 4, tempo); // Formule 4321
// 	playStrumChord([[8,-1,5,5,5 - mode,8]], offset, 2, tempo);

// 	playStrumChord([[-1,5,5,7,6,7], [8,-1,5,5,5 - mode,8], [-1,5,5,7,6,7]], offset, 4, tempo);	// Formule 5151
// 	playStrumChord([[8,-1,5,5,5 - mode,8]], offset, 2, tempo);

// 	if (mode == 0) {
// 		playStrumChord([[-1,7,5,7,5,7], [-1,8,7,10,10,10], [-1,5,5,7,6,7]], offset, 4, tempo);	// Formule 5671
// 		playStrumChord([[8,-1,5,5,5,8]], offset, 2, tempo);

// 		playStrumChord([[-1,8,7,10,10,10], [-1,5,5,7,6,7]], offset, 4, tempo);	// Formule 671
// 		playStrumChord([[8,-1,5,5,5,8]], offset, 2, tempo);

// 		playStrumChord([[-1,5,5,7,6,7]], offset, 4, tempo);	// Formule 71
// 		playStrumChord([[8,-1,5,5,5,8]], offset, 2, tempo);

// 		playStrumChord([[-1,8,7,5,6,8]], offset, 4, tempo);	// Formule 61
// 		playStrumChord([[8,-1,5,5,5,8]], offset, 2, tempo);
// 	} else {
// 		playStrumChord([[-1,6,6,8,7,8], [-1,5,5,7,6,7]], offset, 4, tempo);	// Formule 651
// 		playStrumChord([[8,-1,5,5,5 - mode,8]], offset, 2, tempo);

// 		playStrumChord([[8,-1,5,5,5 - mode,8], [-1,8,7,10,10,10], [-1,5,5,7,6,7], 
// 			[8,-1,5,5,5 - mode,8], [-1,5,5,7,8,6], [-1,6,6,8,7,8]], offset, 4, tempo);	// Formule 5671765
// 		playStrumChord([[-1,5,5,7,6,7]], offset, 2, tempo);

// 		playStrumChord([[-1,8,7,10,10,10], [-1,5,5,7,6,7]], offset, 4, tempo);	// Formule 671
// 		playStrumChord([[8,-1,5,5,5 - mode,8]], offset, 2, tempo);

// 		playStrumChord([[-1,5,5,7,6,7]], offset, 4, tempo);	// Formule 71
// 		playStrumChord([[8,-1,5,5,5 - mode,8]], offset, 2, tempo);		
// 	}
// }
